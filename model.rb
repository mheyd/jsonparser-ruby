# This file contains the classes of the object model.

class Project

  # Create getters for the following instance members:
  attr_reader :expand, :selfProperty, :id, :key, :description, :lead,
    :components, :issueTypes, :assigneeType, :version, :name, :roles,
    :avatarUrls, :projectCategory, :projectTypeKey, :archived

  # Constructor. The passed in parameter has to be valid JSON.
  def initialize json
    @expand = json["expand"]
    @selfProperty = json["self"]
    @id = json["id"]
    @key = json["key"]
    @description = json["description"]
    @lead = Lead.new json["lead"]
    self.components = json["components"]
    self.issueTypes = json["issueTypes"]
    @asigneeType = json["assigneeType"]
    self.versions = json["versions"]
    @name = json["name"]
    @roles = Role.initializeRoles json["roles"]
    @avatarUrls = AvatarUrl.initializeAvatarUrls json["avatarUrls"]
    @projectCategory = ProjectCategory.new json["projectCategory"]
    @projectTypeKey = json["projectTypeKey"]
    @archived = json["archived"]
  end

  # Operator method that is used to create an array of issue types.
  def issueTypes= jsonIssueTypes
    @issueTypes = []
    jsonIssueTypes.each do |jsonIssueType|
      @issueTypes.push IssueType.new jsonIssueType
    end
  end

  # Operator method that is used to create an array of components.
  def components= jsonComponents
    @components = []
    jsonComponents.each do |jsonComponent|
      @components.push Component.new jsonComponent
    end
  end

  # Operator method that is used to create an array of versions.
  def versions= jsonVersions
    @versions = []
    jsonVersions.each do |jsonVersion|
      @versions.push Version.new jsonVersion
    end
  end

end


class Lead

  # Create getters for the following instance members:
  attr_reader :selfProperty, :key, :name, :avatarUrls, :displayName, :active

  # Constructor. The passed in parameter has to be valid JSON.
  def initialize json
    @selfProperty = json["self"]
    @key = json["key"]
    @name = json["name"]
    @avatarUrls = AvatarUrl.initializeAvatarUrls json["avatarUrls"]
    @displayName = json["displayName"]
    @active = json["active"]
  end

end


class AvatarUrl

  # Create getters for the following instance members:
  attr_reader :format, :url

  def initialize format, url
    @format = format
    @url = url
  end

  # Define class method for initializing an array of AvatarUrls.
  def self.initializeAvatarUrls jsonAvatarUrls
    avatarUrls = []
    jsonAvatarUrls.each do |jsonAvatarUrl|
      avatarUrl = AvatarUrl.new jsonAvatarUrl[0], jsonAvatarUrl[1]
      avatarUrls.push avatarUrl
    end
    avatarUrls
  end

end


class Component

  def initialize json
    # The example JSON input does not contain any further information about the
    # structure of components. Therefore no further definitions can be made here.
  end

end


class IssueType

  # Create getters for the following instance members:
  attr_reader :selfProperty, :id, :description, :iconUrl, :name, :subtask, :avatarId

  def initialize json
    @selfProperty = json["self"]
    @id = json["id"]
    @description = json["description"]
    @iconUrl = json["iconUrl"]
    @name = json["name"]
    @subtask = json["subtask"]
    @avatarId = json["avatarId"]
  end

end


class Version

  def initialize json
    # The example JSON input does not contain any further information about the structure of the version.
    # Therefore no further definitions can be made here.
  end

end


class Role

  # Create getters for the following instance members:
  attr_reader :title, :url

  def initialize title, url
    @title = title
    @url = url
  end

  # Define class method for initializing an array of Roles.
  def self.initializeRoles jsonRoles
    roles = []
    jsonRoles.each do |jsonRole|
      role = Role.new jsonRole[0], jsonRole[1]
      roles.push role
    end
    roles
  end

end


class ProjectCategory

  # Create getters for the following instance members:
  attr_reader :selfProperty, :id, :name, :description

  def initialize json
    @selfProperty = json["self"]
    @id = json["id"]
    @name = json["name"]
    @description = json["description"]
  end

end
