# The Ruby JSON package is required.
# Luckily it is part of the Ruby base installation.
require 'json'

# Load the object model classes.
load 'model.rb'

puts "JSONParser - Ruby"

# Load the content of "input.json".
file = File.open "input.json"
json = JSON.load file
file.close

# Initialize the project object from the json data.
project = Project.new json

# Output the names of the project and the project category.
puts "\nThe name of the container project is \"" + project.name + "\"."
puts "The name of the container project category is \"" + project.projectCategory.name + "\"."

# Output the names of all issue types.
puts "\nThe names of the container project issuetypes are:"
project.issueTypes.each do |issueType|
  print "\"" + issueType.name + "\" "
end
puts

# Output the names of all issue types that have a subtask property that is true.
puts "\nThe names of the container project issue types, where subtask is true:"
project.issueTypes.each do |issueType|
  next unless issueType.subtask == true
  print "\"" + issueType.name + "\" "
end

# Uncomment the last line of this file to output an actual representation of the object model.
# This is an easy way to verify that the representation in the memory
# is an actual object oriented model
# puts project.inspect
