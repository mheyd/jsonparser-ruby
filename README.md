# jsonparser-ruby

## What is this?
This is a solution for the task described here: https://mygit.th-deg.de/tlang/jsonparser/-/tree/master



## Why Ruby

Ruby is perfect for setting up a simple and straightforward object model and also very well suited for handling JSON. The base installation of Ruby already contains everything that is needed to quickly and efficiently solve the task at hand.



## Design decisions regarding the object model

After inspecting the input JSON I was not entirely sure how to treat the ***avatarUrls*** and ***roles*** objects: The content of those objects imply that these entities are hash/dictionary like structures, where the user can add new formats for avatar URLs or new role types. So instead of writing just classes that have fixed instance variables for the different formats or the given roles in the input JSON I decided to represent those entities as arrays of objects in the object model.

## How to run it

* Make sure Ruby is installed. The standard installation should already come with everything that is needed. On Debian-based Linux distributions a `sudo apt install ruby` should install everything that is needed. On Windows you can use RubyInstaller: https://rubyinstaller.org/

* Clone or download and unzip the project.

* Go to the directory where the downloaded/cloned project sits on your disk.

* To run the program:
`ruby jsonparser.rb`



## Program output

This is what you should get when you run the program:

```
JSONParser - Ruby

The name of the container project is "123-Musterprojekt".
The name of the container project category is "Softwareprojekt".

The names of the container project issuetypes are:
"Öffentliche FuE-Projekt" "Bearbeitung DM" "Vertragsentwurf" "Vertragspruefung" "Sub-Task"

The names of the container project issue types, where subtask is true:
"Bearbeitung DM" "Vertragsentwurf" "Sub-Task"
```

### Output the whole object model

You can easily get a representation of the object model by uncommenting the last line of jsonparser.rb:

`puts project.inspect`



## Typing and error handling

Ruby should automatically assign the proper types to the object model: So something like

`"subtask": true`


in the input JSON should automatically become a Boolean like type in Ruby.

### Side note about Booleans in Ruby

The concept of Booleans works a bit differently when compared to most other languages, but without going into too much detail: Ruby also knows what true and false is; If you want, for example, to check the type of the ***archived*** instance variable of the Ruby object model, you can add this to jsonparser.rb:

`puts project.archived.class`

### Error handling and types

Ruby is quite robust when it comes to parsing JSON. I did not add any error handling. It's of course possible, to check, for example, if all required properties in the JSON are present and of a certain type - in a more complex program this might be required to ensure everything works properly. It's not at all a problem to ensure the instance variables of Ruby classes are of a certain type by explicitly declaring setters. But since the task here is not very complex, I refrained from bloating up the code too much and relied on the robustness and strong auto typing capabilities of Ruby.



## Circumnavigating the "self" problem

The input JSON contains various objects that have ***self*** properties. Because **self** is a key word in Ruby (the equivalent to **this** in Java or C++/C#). I called those properties ***selfProperty*** in the relevant classes of the object model.



## Contact

If you have any further questions I'm happy to answer them: mh@mheyd.com.
